SELECT
	"market",
	"mazdaSegment",
	"modelYear",
	"makeName",
	"modelName",
	"trimName",
	"msrp",
	"delivery",
	"apr",
	"apr_termMonths",
	"apr_totalValue"
FROM
	RPR_STG.JB_JATO_FINANCEOFFERS jjf
LEFT JOIN RPR_STG.JB_JATO_MODELVERSIONS jjm ON
	jjf."vehicleId" = jjm."vehicleId"