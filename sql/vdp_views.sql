SELECT
	"Date" AS CALENDAR_DATE,
	"RetailerId" AS DEALER_ID,
	CARLINE_CD AS MODEL,
	SUM("VdpViews") AS VDP_VIEWS
FROM
	WP_SHIFT_VDP_DAILY wsvd
INNER JOIN WP_SHIFT_VDP_MODEL_MAPPING wsvmm ON
	UPPER(wsvd."Model") = wsvmm.SHIFT_MODEL
WHERE
	"Status" = 'New'
	AND "Make" = 'Mazda'
	AND "Date" >= DATE '2019-01-03'
GROUP BY "Date", "RetailerId", CARLINE_CD