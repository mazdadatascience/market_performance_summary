SELECT
	"date",
	"dealer",
	MAKE,
	MODEL,
	SEGMENT,
	SUM("sales") AS SALES
FROM
	(
	SELECT
		"date",
		"dealer",
		(
	CASE
			WHEN "make" <> 'mazda' THEN 'OTHERS'
			ELSE 'MAZDA'
		END) AS MAKE,
		(
	CASE
			WHEN MODEL NOT IN ('M3H',
			'M3S',
			'CX3',
			'C30',
			'CX5',
			'CX9',
			'M6G') THEN 'OTHERS'
			ELSE MODEL
		END) AS MODEL,
		SEGMENT,
		"sales"
	FROM
		TM_UB_SOA_DAILY_FINAL
	LEFT JOIN RPR_STG.VIEW_VEH_SEGMENTATION ON
		"make" = lower(BRAND)
		AND (
	CASE
			"model" WHEN 'mazda3 hatchback' THEN 'm3h'
			WHEN 'mazda3 sedan' THEN 'm3s'
			WHEN 'cx-3' THEN 'cx3'
			WHEN 'cx-30' THEN 'c30'
			WHEN 'cx-5' THEN 'cx5'
			WHEN 'cx-9' THEN 'cx9'
			WHEN 'mazda6' THEN 'm6g'
			ELSE "model"
		END) = lower(MODEL)
		AND "model_year" = MODELYEAR
	WHERE
		"date" >= DATE '2019-01-03'
		AND SEGMENT IN ('C CAR',
		'CD CAR',
		'C UTILITY',
		'B UTILITY',
		'MIDSIZE UTILITY 3 ROW',
		'SPORTS CAR')
		AND "retail_fleet" = 'retail'
		AND "dealer" <> 'na') soa_tb
GROUP BY
	"date",
	"dealer",
	MAKE,
	MODEL,
	SEGMENT