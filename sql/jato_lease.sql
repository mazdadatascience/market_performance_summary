SELECT
	"market",
	"mazdaSegment",
	"modelYear",
	"makeName",
	"modelName",
	"trimName",
	"msrp",
	"delivery",
	"annualMileage",
	"residualPercent",
	"moneyFactor",
	"lease_totalValue",
	"lease_termMonths"
FROM
	RPR_STG.JB_JATO_LEASEOFFERS jjl 
LEFT JOIN RPR_STG.JB_JATO_MODELVERSIONS jjm ON
	jjl."vehicleId" = jjm."vehicleId"