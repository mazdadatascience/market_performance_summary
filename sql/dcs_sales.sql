SELECT
	saw_0,
	saw_1,
	saw_2,
	saw_3,
	saw_4,
	saw_5,
	saw_6,
	saw_7,
	saw_8
FROM
	((
	SELECT
		"Sales Calendar & Timing"."Calendar Date" saw_0,
		"Geography"."Market Cd" saw_1,
		'BRAND' saw_2,
		"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
		"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
		"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
		"Calendar Date Calculations"."Segment Sales MTD" saw_6,
		"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
		"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
	FROM
		"Vehicle - Competitor Sales"
	WHERE
		("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
		AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
		AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
		AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )
UNION (
SELECT
	"Sales Calendar & Timing"."Calendar Date" saw_0,
	"Geography"."Retail Region Cd" saw_1,
	'BRAND' saw_2,
	"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
	"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
	"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
	"Calendar Date Calculations"."Segment Sales MTD" saw_6,
	"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
	"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
FROM
	"Vehicle - Competitor Sales"
WHERE
	("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
		AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
			AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
				AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )
UNION (
SELECT
"Sales Calendar & Timing"."Calendar Date" saw_0,
'US' saw_1,
'BRAND' saw_2,
"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
"Calendar Date Calculations"."Segment Sales MTD" saw_6,
"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
FROM
"Vehicle - Competitor Sales"
WHERE
("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
	AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
		AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
			AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )
UNION (
SELECT
"Sales Calendar & Timing"."Calendar Date" saw_0,
"Geography"."Market Cd" saw_1,
CASE
"Competitor Product attributes"."Carline" WHEN 'M3S' THEN 'MAZDA3'
WHEN 'M3H' THEN 'MAZDA3'
WHEN 'C5D' THEN 'CX5'
ELSE "Competitor Product attributes"."Carline"
END saw_2,
"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
"Calendar Date Calculations"."Segment Sales MTD" saw_6,
"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
FROM
"Vehicle - Competitor Sales"
WHERE
("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
	AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
		AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )
UNION (
SELECT
"Sales Calendar & Timing"."Calendar Date" saw_0,
"Geography"."Retail Region Cd" saw_1,
CASE
"Competitor Product attributes"."Carline" WHEN 'M3S' THEN 'MAZDA3'
WHEN 'M3H' THEN 'MAZDA3'
WHEN 'C5D' THEN 'CX5'
ELSE "Competitor Product attributes"."Carline"
END saw_2,
"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
"Calendar Date Calculations"."Segment Sales MTD" saw_6,
"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
FROM
"Vehicle - Competitor Sales"
WHERE
("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
	AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )
UNION (
SELECT
"Sales Calendar & Timing"."Calendar Date" saw_0,
'US' saw_1,
CASE
"Competitor Product attributes"."Carline" WHEN 'M3S' THEN 'MAZDA3'
WHEN 'M3H' THEN 'MAZDA3'
WHEN 'C5D' THEN 'CX5'
ELSE "Competitor Product attributes"."Carline"
END saw_2,
"Calendar Date Calculations"."Mazda Sales MTD" saw_3,
"Calendar Date Calculations"."Mazda Sales Prior Month MTD" saw_4,
"Calendar Date Calculations"."Mazda Sales Prior Year MTD" saw_5,
"Calendar Date Calculations"."Segment Sales MTD" saw_6,
"Calendar Date Calculations"."Segment Sales Prior Month MTD" saw_7,
"Calendar Date Calculations"."Segment Sales Prior Year MTD" saw_8
FROM
"Vehicle - Competitor Sales"
WHERE
("Competitor Product attributes"."Carline" IN ('M3S', 'M3H', 'C30', 'CX3', 'CX5', 'CX9', 'MX5', 'MXR', 'M6G', 'MZ2', 'C5D'))
AND ("Fleet or Non-Fleet Indicator"."Fleet Type Cd" = 'NF')
AND ("Sales Calendar & Timing"."Sales Year" IN (2020, 2021))
AND ("Sales Calendar & Timing"."Calendar Date" <= (CURRENT_DATE - 1)) )) t1