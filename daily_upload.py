import pandas as pd
import os
import subprocess
import datetime

from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
	"oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
	max_identifier_length=128,
)

today_dt = datetime.date.today()

## dim tables
def get_dim_tables():
	#### DLR/MKT ####
	dlr_master = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Dealers"
	)
	dlr_master.to_csv("data/dim_dlr_master.csv", index=False)

	mkt_master = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Markets"
	)
	mkt_master.to_csv("data/dim_mkt_master.csv", index=False)

	#### DATE ####
	yesterday = today_dt - datetime.timedelta(days=1)
	date_master = pd.DataFrame(
		{"cal_date": pd.date_range(start="2019-01-03", end=yesterday)}
	)
	today_str = yesterday.strftime("%Y-%m-%d")
	cal_query = f"WHERE cal_date BETWEEN to_date('2019-01-03', 'yyyy-mm-dd') AND to_date('{today_str}', 'yyyy-mm-dd')"
	cal_master = pd.read_sql(
		f"SELECT cal_date, sales_yr_mo_id, sales_mo_name_yr, sales_mo_total_days FROM RPR_STG.KHN_CALENDAR_MASTER {cal_query}",
		engine,
	)
	date_master = date_master.merge(cal_master, how="left", on="cal_date")
	date_master.to_csv("data/dim_date_master.csv", index=False)

	date_shift = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/Mazdap/date_shift"
	)
	date_shift.to_csv("data/dim_date_shift.csv", index=False)

	#### PARENT/DLR MAPPING ####
	parent_dlr = pd.read_sql("SELECT * FROM WP_PARENT_DEALER_MAPPING", engine)
	parent_dlr.to_csv("data/dim_parent_dlr.csv", index=False)

	#### RE STORES ####
	re_stores = pd.read_sql("SELECT * FROM RPR_STG.TM_RE_STORES", engine)
	re_stores.to_csv("data/dim_re_stores.csv", index=False)

	print("Task: Dimension Tables - Completed.")


## fact tables
def get_sales_and_inventory():
	### DCS Sales ###
	with open("sql/dcs_sales.sql", "r") as dcs_sales_query:
		dcs_sales = conn.exec_sql(
			dcs_sales_query.read(),
			custom_cols=[
				"Calendar Date",
				"Level",
				"Carline",
				"Mazda Sales",
				"Mazda Sales PM",
				"Mazda Sales PY",
				"Segment Sales",
				"Segment Sales PM",
				"Segment Sales PY",
			],
		)
		dcs_sales.to_csv("data/fact_dcs_sales.csv", index=False)

	### Sales Objectives ###
	sls_obj = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Mazda Sales Objectives"
	)
	sls_obj.to_csv("data/fact_sls_objectives.csv", index=False)

	### Mazda Sales ###
	mz_sales = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Mazda GVS Retail Daily"
	)
	mz_sales.to_csv("data/fact_mz_sales.csv", index=False)

	### Mazda Sales Actual ###
	mz_sales_actual = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Mazda GVS Retail Daily (Actual)"
	)
	mz_sales_actual.to_csv("data/fact_mz_sales_actual.csv", index=False)

	### Dealer Stock ###
	mz_inventory = conn.get_csv(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Dealer Stock MTD"
	)
	mz_inventory.to_csv("data/fact_mz_inventory.csv", index=False)

	### Days On Lot ###
	mz_dol = conn.get_excel(
		"/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Mazda DOL MTD",
		skiprows=0,
	)
	mz_dol.to_csv("data/fact_mz_dol.csv", index=False)

	print("Task: Sales and Inventory Fact Tables - Completed.")


def etl_vdp_engaged_visits():
	with open("sql/eng_visits.sql", "r") as eng_query:
		eng_visits = pd.read_sql(eng_query.read(), engine)
		eng_visits.to_csv("data/fact_eng_visits.csv", index=False)

	with open("sql/vdp_views.sql", "r") as vdp_query:
		vdp_views = pd.read_sql(vdp_query.read(), engine)
		vdp_views.to_csv("data/fact_vdp_views.csv", index=False)

	print("Task: VDP Views and Dealer Web Engaged Visits Fact Tables - Completed.")


def etl_soa_share():
	with open("sql/soa_share.sql", "r") as soa_query:
		soa_share_df = pd.read_sql(soa_query.read(), engine)
		soa_share_df.to_csv("data/fact_soa_share.csv", index=False)
	print("Task: SOA Share Fact Table - Completed.")


def etl_jato():
	with open("sql/jato_lease.sql", "r") as lease_query:
		jato_lease = pd.read_sql(lease_query.read(), engine)
		jato_lease.to_csv("data/fact_jato_lease.csv", index=False)

	with open("sql/jato_apr.sql", "r") as apr_query:
		jato_apr = pd.read_sql(apr_query.read(), engine)
		jato_apr.to_csv("data/fact_jato_apr.csv", index=False)
	
	jato_trim = pd.read_sql("SELECT * FROM RPR_STG.JB_JATO_TRIMGROUPS jjt", engine)
	jato_trim.to_csv("data/fact_jato_trim.csv", index=False)
	
	print("Task: Jato Fact Tables - Completed.")


#### MAIN ####
get_dim_tables()
get_sales_and_inventory()
etl_vdp_engaged_visits()
etl_soa_share()
etl_jato()
#### MAIN ####

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/rgtm_summary"
with open("logging.txt", "w") as f:
	process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
	while True:
		return_code = process.poll()
		if return_code is not None:
			break
#### SYNC ONEDRIVE ####

conn.logoff()

